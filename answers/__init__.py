from flask import Flask
from answers.extention_instance import db, csrf, login_manager, mail
from answers.modules.question.view import QuestionView
from answers.modules.user.view import UserView

def init_packages(app):
    """
        init SQLAlchemy instance from models to connect database
        init login manager for user sessions handler
        init csrf token to crypt user posted data
    """
    db.init_app(app)
    login_manager.init_app(app)
    csrf.init_app(app)
    mail.init_app(app)


def register_views(app):
    """
        Register views
    """
    QuestionView.register(app)
    UserView.register(app)

def create_app():
    """
        create Flask application
        collect all current settings
        register utilities and project routes
        :return: app
    """
    app = Flask('answers')
    app.config.from_object('answers.settings')
    init_packages(app)
    register_views(app)

    return app
