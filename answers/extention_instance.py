from flask_sqlalchemy import SQLAlchemy
from flask_wtf import CsrfProtect
from flask.ext.login import LoginManager
from flask.ext.mail import Mail


"""
create instance to avoid cyclic dependencies
"""

csrf = CsrfProtect()
db = SQLAlchemy()
mail = Mail()


"""   LOGIN CONFIG   """
from answers.models import User
login_manager = LoginManager()
login_manager.login_view = 'login'

@login_manager.user_loader
def load_user(id):
    if id == None or id == 'None': id = -1
    return User.query.get(int(id))
