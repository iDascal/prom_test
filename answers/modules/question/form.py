from flask.ext.wtf import Form
from wtforms import StringField, validators, PasswordField, SubmitField, ValidationError, BooleanField, TextAreaField
from wtforms.validators import DataRequired, Length, Email, Regexp, EqualTo
from answers.models import User


class AddQuestionForm(Form):

    subject = TextAreaField('Subject', validators=[DataRequired(), Length(1, 500)])
    content = TextAreaField('Subject', validators=[DataRequired()])

    submit = SubmitField('Submit')


class AddAnswerForm(Form):

    content = TextAreaField('Subject', validators=[DataRequired()])

    submit = SubmitField('Submit')
