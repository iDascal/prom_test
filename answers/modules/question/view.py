from flask import render_template, redirect, url_for, request
from flask.ext.login import current_user, login_required
from flask_classy import FlaskView, route
from answers import db
from answers.models import Question, Answer
from answers.modules.question.form import AddQuestionForm, AddAnswerForm


class QuestionView(FlaskView):
    """
    Handle request on start page
    """

    @route('/<question_id>', endpoint='question', methods=['GET', 'POST'])
    def question(self, question_id):
        question = Question.query.get(question_id)

        return render_template('question.html', question=question)

    @route('/add_question', endpoint='add_question', methods=['GET', 'POST'])
    @login_required
    def add_question(self):
        form = AddQuestionForm()

        if form.validate_on_submit():
            question = Question()
            question.subject = form.subject.data
            question.content = form.content.data
            question.user_id = current_user.id

            db.session.add(question)
            db.session.commit()

            return redirect(url_for('main'))

        return render_template('add_question_b.html', form=form)

    @route('/add_answer/<question_id>', endpoint='add_answer', methods=['GET', 'POST'])
    @login_required
    def add_answer(self, question_id):
        form = AddAnswerForm()
        question = Question.query.get(question_id)
        if form.validate_on_submit():
            answer = Answer()
            answer.content = form.content.data
            answer.user_id = current_user.id
            answer.question_id = question_id

            db.session.add(answer)
            db.session.commit()

            return redirect(url_for('question', question_id=question_id))

        return render_template('add_answer_b.html', form=form, question=question)

    @route('/voting', endpoint='voting', methods=['GET', 'POST'])
    @login_required
    def voting(self):
        answer_id = request.args.get('answer_id', None)
        question_id = request.args.get('question_id', None)

        answer = Answer.query.get(answer_id)
        answer.vote_up()
        db.session.add(answer)
        db.session.commit()

        return redirect(url_for('question', question_id=question_id))
