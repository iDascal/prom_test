from flask import redirect, url_for, flash, render_template, request
from flask.ext.login import current_user, login_user, logout_user, login_required
from flask_classy import FlaskView, route
from werkzeug.security import generate_password_hash
from answers import db, mail
from answers.models import User, Question
from answers.modules.user.form import LoginForm, RegistrationForm
from flask.ext.mail import Message
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from answers.settings import SECRET_KEY


class UserView(FlaskView):
    """
    Handle request on start page
    """
    route_base = '/'
    @route('/', endpoint='main')
    def user(self):
        questions = Question.query.all()
        # return render_template('index.html')
        return render_template('main.html', questions=questions)

    @route('/register', endpoint='register', methods=['GET', 'POST'])
    def registration(self):
        form = RegistrationForm()
        if form.validate_on_submit():
            password_hash = generate_password_hash(form.password.data)
            user = User(email=form.email.data,
                        user_name=form.username.data,
                        password_hash=password_hash)
            db.session.add(user)
            db.session.commit()
            # token = user.generate_confirmation_token()
            # confirm_url = url_for('confirm', token=token, _external=True)
            # message = Message('Confirm Your email', recipients=['tihiy@ukr.net'])
            # # message = Message('Confirm Your email', recipients=[form.email.data])
            # message.html = render_template('test_confirm_email.html', user=user, token=confirm_url)
            # mail.send(message)
            login_user(user)
            return redirect('/')

            # return render_template('test.html', data='Thank you! Check your email.')

        return render_template('registration_b.html', form=form)

    @route('/confirm/<token>', endpoint='confirm')
    def confirm(self, token):
        s = Serializer(SECRET_KEY)
        try:
            data = s.loads(token)
        except:
            return False

        user = User.query.get(data.get('user_auth42'))
        user.confirmed = True
        login_user(user)
        db.session.add(user)
        db.session.commit()
        return redirect('/')

    @route('/login', endpoint='login', methods=['GET', 'POST'])
    def login(self):
        form = LoginForm()

        if form.validate_on_submit():
            # user = User.query.filter_by(email=form.email.data).first()
            #
            # if user is None:
            #     self.email.errors.append('invalid user')
            #     return False
            # if user is None:
            #     return render_template('test_login_message.html', data='Invalid user')
            #
            # if not user.verify_password(form.password.data):
            #     return render_template('test_login_message.html', data='Invalid password')
            #
            # if user.confirmed is False:
            #     return render_template('test_login_message.html', data='Confirm your email')

            login_user(form.user, form.remember_me.data)
            return redirect(request.args.get('next') or url_for('main'))
        # return render_template('login.html', form=form)
        return render_template('login_b.html', form=form)
    @route('/logout', endpoint='logout')
    @login_required
    def logout(self):
        logout_user()
        return redirect(url_for('main'))
