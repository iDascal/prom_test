from flask.ext.login import UserMixin
from werkzeug.security import check_password_hash
from answers.extention_instance import db
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from answers.settings import SECRET_KEY


class User(db.Model, UserMixin):
    __tablename__ = 'users'

    id = db.Column('user_id', db.Integer, primary_key=True)
    user_name = db.Column('user_name', db.String(200))
    email = db.Column('user_email', db.String(50))
    questions = db.relationship('Question', backref='user')
    answers = db.relationship('Answer', backref='user')
    password_hash = db.Column('user_password_hash', db.String(256))
    confirmed = db.Column('user_email_confirmed', db.Boolean, default=False)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_confirmation_token(self, expiration=3600):
        s = Serializer(SECRET_KEY, expiration)
        return s.dumps({'user_auth42': self.id})

    def __str__(self):
        return '{} => {}'.format(self.id, self.user_name)

class Question(db.Model):
    __tablename__ = 'question'

    id = db.Column('question_id', db.Integer, primary_key=True)
    subject = db.Column('question_subject', db.String(500))
    content = db.Column('question_content', db.Text())
    created = db.Column('question_created', db.DateTime, default=db.func.now())
    user_id = db.Column('question_user_id', db.Integer, db.ForeignKey('users.user_id'))
    answers = db.relationship('Answer', backref='question')

    def __str__(self):
        return '{} => {} => {}'.format(self.id, self.subject, self.content)

class Answer(db.Model):
    __tablename__ = 'answer'
    id = db.Column('answer_id', db.Integer, primary_key=True)
    content = db.Column('answer_content', db.Text())
    created = db.Column('answer_created', db.DateTime, default=db.func.now())
    votes = db.Column('answer_votes', db.Integer, default=0)
    user_id = db.Column('answer_user_id', db.Integer, db.ForeignKey('users.user_id'))
    question_id = db.Column('answer_question_id', db.Integer, db.ForeignKey('question.question_id'))

    def vote_up(self):
        self.votes += 1
        return self.votes

    def __str__(self):
        return '{} => {} => {} => {}'.format(self.id, self.user_id, self.question_id, self.content)
