from answers import db
from run import app
from flask_migrate import Manager, Migrate, MigrateCommand
from answers.models import User

# Migration script, do not touch
# Create and work with 'migrations' folder
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)


if __name__ == '__main__':
    manager.run()
