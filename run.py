import os
from answers import create_app

app = create_app()


if __name__ == '__main__':
    app.run(host='localhost', port=5000, debug=True)
     # port = int(os.environ.get('PORT', 5000))
     # app.run(host='0.0.0.0', debug=True)
